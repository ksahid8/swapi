import React from "react";

const GridCardSkeleton = () => {
  return (
    <div className="flex w-2/6 h-auto bg-[#27272a] rounded-xl p-6 gap-1.5 max-w-[32%] flex-col">
      <div className="font-thin text-[#fbcb56] animate-pulse bg-[#767678] w-full h-6 rounded-lg"></div>
      <div className="flex items-center">
        <div className="w-14">
          <div className="bg-[#767678] h-9 w-9 rounded-full text-[#f7f7f7] leading-9 text-center animate-pulse"></div>
        </div>
        <div className="w-3/4 mr-4">
          <div className="text-[#f7f7f7] animate-pulse bg-[#767678] w-full h-4 rounded-lg mb-2"></div>
          <div className="text-[#a1a2a9] capitalize animate-pulse bg-[#767678] w-full h-4 rounded-lg"></div>
        </div>
        <div className="bg-[#767678] h-9 w-9 rounded-full text-[#f7f7f7] leading-9 text-center animate-pulse"></div>
      </div>
      <div className="text-[#e4e4e6] animate-pulse bg-[#767678] w-full h-6 rounded-lg"></div>
    </div>
  );
};

export default GridCardSkeleton;
