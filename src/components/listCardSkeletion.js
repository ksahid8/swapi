import React from "react";

const ListCardSkeleton = () => {
  return (
    <div className="bg-[#27272a] rounded-xl px-10 py-6 pb-8 flex justify-between">
      <div className="flex flex-col gap-2.5 flex-auto">
        <div className="text-[#fbcb56] font-thin animate-pulse bg-[#767678] w-full h-6 rounded-lg"></div>
        <div className="flex gap-3.5">
          <div className="bg-[#767678] w-12 h-12 flex justify-center rounded-xl items-center animate-pulse"></div>
          <div className="flex flex-col gap-0.7 flex-auto">
            <div className="text-[#f5f5f5] bg-[#767678] text-lg font-medium animate-pulse w-full h-6 rounded-lg mb-2"></div>
            <div className="text-[#7d7d84] bg-[#767678] text-md font-normal animate-pulse w-full h-6 rounded-lg"></div>
          </div>
        </div>
      </div>
      <div className="w-6/12 flex flex-col items-end gap-2.5">
        <div className="bg-[#767678] text-md font-normal animate-pulse w-full h-6 rounded-lg w-20"></div>
        <div className="bg-[#767678] text-md font-normal animate-pulse w-full h-6 rounded-lg w-40"></div>
      </div>
    </div>
  );
};

export default ListCardSkeleton;
