import React from "react";
import moment from "moment";
import { BiPlanet } from "react-icons/bi";
import { GiPlanetCore } from "react-icons/gi";
import { IoIosArrowDown } from "react-icons/io";

const ListCard = (props) => {
  const { PlanetDetails } = props;

  const { name, created, climate, films, population } = PlanetDetails;

  return (
    <div className="bg-[#27272a] rounded-xl px-10 py-6 pb-8 flex justify-between cursor-pointer transition ease-in-out delay-100  hover:-translate-y-1 hover:scale-30  duration-300 hover:shadow-[0px_3px_20px_#00000054]">
      <div className="flex flex-col gap-2.5">
        <div className="text-[#fbcb56]  font-thin">
          {moment(created).format("MMM D YYYY")}
        </div>
        <div className="flex gap-3.5">
          <div className="bg-[#3f3f46] min-w-[3rem] h-12 flex justify-center rounded-xl items-center">
            {+population.length >= 2 && population !== "unknown" ? (
              <BiPlanet color="#fbcb56" size={24} />
            ) : (
              <GiPlanetCore color="#fbcb56" size={24} />
            )}
          </div>
          <div className="flex flex-col gap-0.7">
            <div className="text-[#f5f5f5] text-lg font-medium">{name}</div>
            <div className="text-[#7d7d84] text-md font-normal">
              {films.join(", ")}
            </div>
          </div>
        </div>
      </div>
      <div className="w-6/12 flex flex-col items-end gap-2.5">
        <div className="text-[#fbcb56] font-thin">
          {moment(created).format("h:mm A")}
        </div>
        <div className="text-[#5b5b63] capitalize">{climate}</div>
        <IoIosArrowDown color="#ededee" size={20} />
      </div>
    </div>
  );
};

export default ListCard;
