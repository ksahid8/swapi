import React from "react";
import moment from "moment";
import { BiPlanet } from "react-icons/bi";
import { GiPlanetCore } from "react-icons/gi";

const GridCard = (props) => {
  const { PlanetDetails } = props;

  const { name, created, climate, films, population } = PlanetDetails;
  return (
    <div className="flex w-2/6 h-auto bg-[#27272a] rounded-xl p-6 gap-1.5 flex-col max-w-[32%] cursor-pointer transition ease-in-out delay-100  hover:-translate-y-1 hover:scale-30  duration-300 hover:shadow-[0px_3px_20px_#00000054]">
      <div className="font-thin text-[#fbcb56]">
        {moment(created).format("MMM D YYYY h:mm A")}
      </div>
      <div className="flex items-center">
        <div className="w-14">
          <div className="bg-[#767678] h-9 w-9 rounded-full text-[#f7f7f7] leading-9 text-center">
            {name[0]}
          </div>
        </div>
        <div className="w-3/4">
          <div className="text-[#f7f7f7]">{name}</div>
          <div className="text-[#a1a2a9] capitalize">{climate}</div>
        </div>
        <div>
          {+population.length >= 2 && population !== "unknown" ? (
            <BiPlanet color="#fbcb56" size={30} />
          ) : (
            <GiPlanetCore color="#fbcb56" size={30} />
          )}
        </div>
      </div>
      <div className="text-[#e4e4e6]">{films.join(", ")}</div>
    </div>
  );
};

export default GridCard;
