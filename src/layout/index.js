import React from "react";
import Header from "./header";

const Layout = (props) => {
  return (
    <div className="flex flex-col">
      <Header />
      <div className="min-h-screen bg-[#3e3e45]">{props.children}</div>
    </div>
  );
};

export default Layout;
