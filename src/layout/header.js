import React, { useContext } from "react";
import { BsGridFill } from "react-icons/bs";
import { FaListUl } from "react-icons/fa";
import { AiOutlineFilter } from "react-icons/ai";
import { GlobalState } from "./../context-api";
import { Bars } from "react-loading-icons";

const Header = () => {
  const { GridView, setGridView, IsFiltered, setIsFiltered, Loading } =
    useContext(GlobalState);

  const filterButtonText = () => {
    if (Loading) {
      if (IsFiltered === "true") {
        return "Fetching Filtered Planets";
      } else {
        return "Fetching All Planets";
      }
    } else {
      if (IsFiltered === "true") {
        return "Fetch All Planets";
      } else {
        return "Fetch Filtered Planets";
      }
    }
  };

  return (
    <div className="h-16 flex justify-between px-12 items-center bg-[#27272a] text-white">
      <div className="text-[#fbcb56] text-xl">SWAPI</div>
      <div className="flex">
        <button
          className={`py-1.5 px-5 bg-[#fbcb56] hover:bg-[#be9738] text-[#27272a]  rounded-xl  items-center gap-1.5 flex  mr-5 ${
            Loading ? "cursor-wait" : "cursor-pointer"
          }`}
          onClick={() => {
            if (!Loading) {
              setIsFiltered(IsFiltered === "true" ? "false" : "true");
            }
          }}
        >
          {Loading ? (
            <Bars className="h-4 w-auto" fill="#27272a" />
          ) : (
            <AiOutlineFilter />
          )}
          {filterButtonText()}
        </button>
        <button
          className="py-1.5 px-5 bg-[#3e3e45] text-[#f7f7f7] hover:text-[#fbcb56]  rounded-xl items-center gap-1.5 flex"
          onClick={() => {
            setGridView(!GridView);
          }}
        >
          {!GridView ? <BsGridFill /> : <FaListUl />}
          {!GridView ? "Grid View" : "List View"}
        </button>
      </div>
    </div>
  );
};

export default Header;
