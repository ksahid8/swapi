import React, { createContext, useState } from "react";
import { FetchFilteredData, FetchData } from "./utils/functions";

export const GlobalState = createContext();

export const GlobalStateProvider = (props) => {
  const [Data, setData] = useState([]);
  const [Loading, setLoading] = useState(false);
  const [GridView, setGridView] = useState(false);
  const [IsFiltered, setIsFiltered] = useState("true");

  return (
    <GlobalState.Provider
      value={{
        FetchData,
        FetchFilteredData,
        Data,
        setData,
        Loading,
        setLoading,
        GridView,
        setGridView,
        IsFiltered,
        setIsFiltered,
      }}
    >
      {props.children}
    </GlobalState.Provider>
  );
};
