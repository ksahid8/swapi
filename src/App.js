import Landing from "./containers";
import React from "react";
import { GlobalStateProvider } from "./context-api";
import { Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-oldschool-dark";
import { alertOptions } from "./utils/constants";

const App = () => {
  return (
    <>
      <GlobalStateProvider>
        <AlertProvider template={AlertTemplate} {...alertOptions}>
          <Landing />
        </AlertProvider>
      </GlobalStateProvider>
    </>
  );
};

export default App;
