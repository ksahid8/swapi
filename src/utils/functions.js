import { getPlanets, getFilms, getSpecies, getDetails } from "./services";

export const FetchData = async (setLoading, setData, alert) => {
  setLoading(true);

  let AllPlanets = [];
  const allFilms = new Promise((resolve, reject) => {
    (async () => {
      let page = 1;
      let fetchedFilms = [];
      let totalPages = 0;
      await getFilms(page).then((res) => {
        totalPages = Math.ceil(res?.count / 10);
        res?.results.map((item) => {
          fetchedFilms.push(item);
        });
      });

      if (totalPages > 1) {
        for (let i = 2; i <= totalPages; i++) {
          await getFilms(i).then((res) => {
            res?.results.map((item) => {
              fetchedFilms.push(item);
            });
          });
        }
      }
      resolve(fetchedFilms);
    })();
  });

  const allPlanets = new Promise((resolve, reject) => {
    (async () => {
      let page = 1;
      let fetchedPlanets = [];
      let totalPages = 0;
      await getPlanets(page).then((res) => {
        totalPages = Math.ceil(res?.count / 10);
        res?.results.map((item) => {
          fetchedPlanets.push(item);
        });
      });

      if (totalPages > 1) {
        for (let i = 2; i <= totalPages; i++) {
          await getPlanets(i).then((res) => {
            res?.results.map((item) => {
              fetchedPlanets.push(item);
            });
          });
        }
      }
      resolve(fetchedPlanets);
    })();
  });

  await Promise.all([allFilms, allPlanets]).then(async (results) => {
    const allFilms = await results[0];
    const allPlanets = await results[1];
    await Promise.all(
      await allPlanets.map(async (planet) => {
        let planetFilms = [];
        if (planet?.films?.length > 0) {
          await planet.films.map(async (film) => {
            await allFilms.map(async (item) => {
              if (item?.url === film) {
                await planetFilms.push(item?.title);
              }
            });
          });
          planet.films = await planetFilms;
        }
        await AllPlanets.push(planet);
      })
    );
    await alert.show("Fetched All Planets");
    await setLoading(false);
    await setData(AllPlanets);
  });
};

export const FetchFilteredData = async (setLoading, setData, alert) => {
  setLoading(true);

  let FilteredPlanets = [];
  const allFilms = new Promise((resolve, reject) => {
    (async () => {
      let page = 1;
      let fetchedFilms = [];
      let totalPages = 0;
      await getFilms(page).then((res) => {
        totalPages = Math.ceil(res?.count / 10);
        res?.results.map((item) => {
          fetchedFilms.push(item);
        });
      });

      if (totalPages > 1) {
        for (let i = 2; i <= totalPages; i++) {
          await getFilms(i).then((res) => {
            res?.results.map((item) => {
              fetchedFilms.push(item);
            });
          });
        }
      }
      resolve(fetchedFilms);
    })();
  });

  const allSpecies = new Promise((resolve, reject) => {
    (async () => {
      let page = 1;
      let fetchedSpecies = [];
      let totalPages = 0;
      await getSpecies(page).then((res) => {
        totalPages = Math.ceil(res?.count / 10);
        res?.results.map((item) => {
          fetchedSpecies.push(item);
        });
      });

      if (totalPages > 1) {
        for (let i = 2; i <= totalPages; i++) {
          await getSpecies(i).then((res) => {
            res?.results.map((item) => {
              fetchedSpecies.push(item);
            });
          });
        }
      }
      resolve(fetchedSpecies);
    })();
  });

  await Promise.all([allFilms, allSpecies]).then(async (results) => {
    const allFilms = await results[0];
    const allSpecies = await results[1];
    await Promise.all(
      await allSpecies.map(async (spec) => {
        if (
          spec?.films?.length > 0 &&
          spec?.people?.length > 0 &&
          spec?.homeworld &&
          spec?.classification === "reptile"
        ) {
          let planetObject = await getDetails(spec?.homeworld);
          let planetFilms = [];
          if (spec?.films?.length > 0) {
            await spec.films.map(async (film) => {
              await allFilms.map(async (item) => {
                if (item?.url === film) {
                  await planetFilms.push(item?.title);
                }
              });
            });

            planetObject.films = await planetFilms;
          }
          await FilteredPlanets.push(planetObject);
        }
      })
    );
    await alert.show("Fetched Filtered Planets");
    await setLoading(false);
    await setData(FilteredPlanets);
  });
};
