import { getData } from "./apiCall";

export async function getPlanets(page) {
  return await getData(`/planets/?page=${page}`).then((response) => {
    if (response) {
      return response;
    }
  });
}

export async function getSpecies(page) {
  return await getData(`/species/?page=${page}`).then((response) => {
    if (response) {
      return response;
    }
  });
}

export async function getFilms(page) {
  return await getData(`/films/?page=${page}`).then((response) => {
    if (response) {
      return response;
    }
  });
}

export async function getDetails(url) {
  return await getData(url).then((response) => {
    if (response) {
      return response;
    }
  });
}
