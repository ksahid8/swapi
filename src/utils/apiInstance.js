import axios from "axios";
const axiosApiInstance = axios.create({
  baseURL: "https://swapi.dev/api",
});

axiosApiInstance.interceptors.request.use(
  async (config) => {
    config.headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

export default axiosApiInstance;
