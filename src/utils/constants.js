import GridCardSkeleton from "../components/gridCardSkeleton";
import ListCardSkeleton from "../components/listCardSkeletion";
import { transitions, positions } from "react-alert";

export const GridSkeletonLoader = [...Array(9).keys()].map((item, index) => (
  <GridCardSkeleton key={index} />
));

export const ListSkeletonLoader = [...Array(9).keys()].map((item, index) => (
  <ListCardSkeleton key={index} />
));

export const alertOptions = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: "30px",
  transition: transitions.SCALE,
};
