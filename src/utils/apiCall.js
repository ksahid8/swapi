import axiosApiInstance from "./apiInstance";

async function sendApiRequest(url) {
  let response = await axiosApiInstance
    .get(url)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log(error);
    });

  if (response?.status === 200) return response?.data;
}

export async function getData(url) {
  return sendApiRequest(url, {}, "GET");
}
