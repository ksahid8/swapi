import React, { useEffect, useState, useContext } from "react";
import ListCard from "../components/listCard";
import GridCard from "../components/gridCard";
import { GridSkeletonLoader, ListSkeletonLoader } from "../utils/constants";
import Layout from "../layout";
import { GlobalState } from "./../context-api";
import { useAlert } from "react-alert";

const Landing = () => {
  const alert = useAlert();
  const {
    IsFiltered,
    FetchData,
    FetchFilteredData,
    Data,
    setData,
    Loading,
    setLoading,
    GridView,
  } = useContext(GlobalState);

  useEffect(() => {
    if (IsFiltered === "true") {
      FetchFilteredData(setLoading, setData, alert);
    } else if (IsFiltered === "false") {
      FetchData(setLoading, setData, alert);
    }
  }, [IsFiltered]);

  return (
    <Layout>
      <div
        className={`flex px-12 py-6  gap-4 ${
          GridView ? "flex-row flex-wrap" : "flex-col"
        }`}
      >
        {GridView
          ? Loading
            ? GridSkeletonLoader
            : Data.map((planet, index) => (
                <GridCard key={index} PlanetDetails={planet} />
              ))
          : Loading
          ? ListSkeletonLoader
          : Data.map((planet, index) => (
              <ListCard key={index} PlanetDetails={planet} />
            ))}
      </div>
    </Layout>
  );
};

export default Landing;
